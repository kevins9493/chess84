package chess;

import pieces.ChessPiece;
import pieces.Pawn;
import pieces.Rook;

import java.util.List;
import java.util.Scanner;

import pieces.Bishop;
import pieces.King;
import pieces.Queen;
import pieces.Knight;

/**
 * Chess84 main class
 * @author Kevin Slachta
 * @author Leon J Kim
 *
 */
public class Chess {
	final static int BLACKMOVE = 1;
	final static int WHITEMOVE = 0;
	
	Scanner scanner = new Scanner(System.in);
	int move = 0;
	private boolean whitecheck = false;
	private boolean blackcheck = false;
	private boolean WhiteCheckmate = false;
	private boolean BlackCheckmate = false;
	private boolean EnPassantOkBLACK = false;
	private boolean EnPassantOkWHITE = false;
	private boolean cancastle = false;
	private String promotionstring = "";
	private boolean drawquestion = false;
	private boolean iswon = false;
	ChessPiece[][] boards = new ChessPiece[9][9];
	public static void main (String args[]) {
		new Chess();
	}
	public Chess() {
		generateBoard();
		//generateCheck();
		printBoard();
		while (iswon == false) {
			stalemate();
			getInput();
			promotion();
			printBoard();
		}
		scanner.close();
	}
	/**
	 * This method is used to check if stalemate occurs before player input is accepted.
	 * Checks if any piece on the board has any moves returned from getMoves() or 
	 * in the case of a king, checks if he has any moves that would not place him in check
	 * @return void this method returns when stalemate has not occured 	 
	 */
	private void stalemate() {
		if (move == ChessPiece.BLACK && blackcheck == true)
			return;
		if (move == ChessPiece.WHITE && whitecheck == true)
			return;
		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 9; j++) {
				if (boards[i][j] != null && boards[i][j].getColor() == move) {
					if (boards[i][j].toString().equals("bK") || boards[i][j].toString().equals("wK")) {
						List<String> kingpossibilites = boards[i][j].getMoves(boards);
						for (int k = 1; k < 9; k++) {
							for (int l = 1; l < 9; l++) {
								if (boards[k][l] != null)
									if (boards[k][l].getColor() != move) {
										List<String> piecepossibilies = boards[k][l].getMoves(boards);
										for (String s: piecepossibilies) {
											if (kingpossibilites.contains(s)) {
												kingpossibilites.remove(s);
											}
										}
									}
							}
						}
						if (!kingpossibilites.isEmpty()) {
							return;
						}
					} else if (!boards[i][j].getMoves(boards).isEmpty()) {
						return;
					}
				}
			}
		}
		System.out.println("Stalemate");
		System.exit(0);
	}
	/**
	 * This method checks the backline of both sides of the board for opposite color pieces
	 * and replaces them with the user defined piece, if no piece is defined or invalid input
	 * it defaults to queen
	 * @return nothing
	 */
	private void promotion() {
		ChessPiece promotionpiece = null;
		
		for (int i = 1; i < 9; i++) {
			if (boards[i][8] != null && boards[i][8].toString().equals("wp")) {
				if (promotionstring.equals("Q"))
					promotionpiece = new Queen((char) (i + 96) + "8", ChessPiece.WHITE);
				if (promotionstring.equals("R"))
					promotionpiece = new Rook((char) (i + 96) + "8", ChessPiece.WHITE);
				if (promotionstring.equals("B"))
					promotionpiece = new Bishop((char) (i + 96) + "8", ChessPiece.WHITE);
				if (promotionstring.equals("N"))
					promotionpiece = new Knight((char) (i + 96) + "8", ChessPiece.WHITE);
				if (promotionpiece == null)
					promotionpiece = new Queen((char) (i + 96) + "8", ChessPiece.WHITE);
				
				boards[i][8] = promotionpiece;
			}
		}

		for (int i = 1; i < 9; i++) {
			if (boards[i][1] != null && boards[i][1].toString().equals("bp")) {
				if (promotionstring.equals("Q"))
					promotionpiece = new Queen((char) (i + 96) + "1", ChessPiece.BLACK);
				if (promotionstring.equals("R"))
					promotionpiece = new Rook((char) (i + 96) + "1", ChessPiece.BLACK);
				if (promotionstring.equals("B"))
					promotionpiece = new Bishop((char) (i + 96) + "1", ChessPiece.BLACK);
				if (promotionstring.equals("N"))
					promotionpiece = new Knight((char) (i + 96) + "1", ChessPiece.BLACK);
				if (promotionstring == null)
					promotionpiece = new Queen((char) (i + 96) + "1", ChessPiece.BLACK);
				
				boards[i][1] = promotionpiece;
			}
		}
		promotionstring = "";
	}
	/**
	 * Check() is used to determine if the is still in check post user move.
	 * I.E if you are already in check and attempt to move to another square
	 * that would keep you in check.
	 * @return nothing
	 */
	private void check() {
		String location = "";
		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 9; j++) {
				if (boards[i][j] != null)
					if (move == BLACKMOVE && boards[i][j].toString().equals("wK")) {
						location = boards[i][j].getLocation();
						break;
					} else if (move == WHITEMOVE && boards[i][j].toString().equals("bK")) {
						location = boards[i][j].getLocation();
						break;
					}
			}
		}
		
		
		List<String> possibilities;
		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 9; j++) {
				if (boards[i][j] != null) {
					//check piece color first
					if (boards[i][j].getColor() == move) {
						possibilities = boards[i][j].getMoves(boards);
						for (String s : possibilities) {
							//System.out.println("KK: " + s + " : " + location);
							if (s.equals(location)) {
								if (move == WHITEMOVE) {
									blackcheck = true;
									break;
								} else {
									whitecheck = true;
									break;
								}
							}
						}
					}
				}
			}
		}
	}
	/**
	 * Pre-user input checkagain() checks if the king is currently in check.
	 * @return nothing
	 */
	private void checkagain() {
		String location = "";
		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 9; j++) {
				if (boards[i][j] != null)
					if (move == WHITEMOVE && boards[i][j].toString().equals("wK")) {
						location = boards[i][j].getLocation();
						break;
					} else if (move == BLACKMOVE && boards[i][j].toString().equals("bK")) {
						location = boards[i][j].getLocation();
						break;
					}
			}
		}
		
		blackcheck = false;
		whitecheck = false;
		
		List<String> possibilities;
		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 9; j++) {
				if (boards[i][j] != null) {
					//check piece color first
					if (boards[i][j].getColor() != move) {
						possibilities = boards[i][j].getMoves(boards);
						for (String s : possibilities) {
							//System.out.println("KK: " + s + " : " + location);
							if (s.equals(location)) {
								if (move == BLACKMOVE) {
									blackcheck = true;
									break;
								} else {
									whitecheck = true;
									break;
								}
							}
						}
					}
				}
			}
		}
	}
	/**
	 * checkmate() calculated whether there are any valid moves left in the game for a player
	 * and sets checkmate = true if there are none
	 * @return nothing
	 */
	private void checkmate() {
		List<String> possibilities;
		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 9; j++) {
				if (boards[i][j] != null) {
					//check piece color first
					if (boards[i][j].getColor() == move) {
						possibilities = boards[i][j].getMoves(boards);
						ChessPiece temp = null;
						for (String s : possibilities) {
//							if (s.equals(location)) {
//								if(move == BLACKMOVE && blackcheck == true) {
//								location = boards[i][j].getLocation();}
//								break;
//							} else if (move == WHITEMOVE && whitecheck == true) {
//								location = boards[i][j].getLocation();
//								break;
//							}
							int potentialx = (int) s.charAt(0) % 96;
							int potentialy = Character.getNumericValue(s.charAt(1));
							temp = boards[potentialx][potentialy];
							boards[potentialx][potentialy] = boards[i][j];
							boards[i][j] = null;
							boards[potentialx][potentialy].setLocation(potentialy, potentialx, s.charAt(0));
							checkagain();
							if (move == ChessPiece.WHITE && whitecheck == true) {
								//System.out.println("\nsIllegal move, try again\n");
								boards[i][j] = boards[potentialx][potentialy];
								boards[potentialx][potentialy] = temp;
								boards[i][j].setLocation(i, j, s.charAt(0));
							}
							if (move == ChessPiece.BLACK && blackcheck == true) {
								//System.out.println("\nssIllegal move, try again\n");
								boards[i][j] = boards[potentialx][potentialy];
								boards[potentialx][potentialy] = temp;
								boards[i][j].setLocation(i, j, s.charAt(0));							
							}
						}
					}
				}
			}
		}
		if (move == ChessPiece.WHITE && whitecheck == true) {
			System.out.println("\nBlack wins");
			iswon = true;
			System.exit(0);
		}
		if (move == ChessPiece.BLACK && blackcheck == true) {
			System.out.println("\nWhite wins");
			iswon = true;
			System.exit(0);
		}
	}
	/**
	 * getInput() is used to parse the user console input commands
	 * getInput() will recurse inward until a valid move is selected by the user
	 * and then will unwind and return to the gameplay loop
	 * @return nothing
	 */
	private void getInput() {
		if (move == WHITEMOVE)
			System.out.print("White's move: ");
		else if (move == BLACKMOVE)
			System.out.print("Black's move: ");
		boolean correctinput = false;
		String input = "";
		String[] holder;
		int startx, starty, endx, endy;
		ChessPiece temp = null;
		while (correctinput == false) {
			input = scanner.nextLine();
			if (drawquestion == true && input.equals("draw")) {
				System.out.println("Draw");
				System.exit(0);
			} else if (drawquestion == true) {
				drawquestion = false;
			}
			if (input.equals("resign")) {
				System.out.println("Over");
				iswon = true;
				break;
			}
			holder = input.split(" ");
			startx = (int) holder[0].charAt(0) % 96;
			endx = (int) holder[1].charAt(0) % 96;
			starty = Character.getNumericValue(holder[0].charAt(1));
			endy = Character.getNumericValue(holder[1].charAt(1));
			if (input.contains("get")) {
				List<String> test = boards[endx][endy].getMoves(boards);
				for (String s: test) {
					System.out.println("MOVES POSSIBLE: " + boards[endx][endy].toString() + " to " + s);
				}
				break;
			}
			if (holder.length == 3) {
				if (holder[2].equals("draw?")) {
					drawquestion = true;
				} else {
					promotionstring = holder[2];
				}
			}
			/**
			 * Check if piece is yours - DONE
			 * Check if space selected is valid - DONE
			 * Check if piece can make selected move - DONE
			 * If in check ensure move breaks check, otherwise fail
			 * Make move, switch users - DONE
			 */
			//System.out.println("startx: " + startx + " starty: " + starty + " Endsx: " + endx + " Endy: " + endy);
			//System.out.println("SSS:" + boards[startx][starty].toString());
			if (startx > 8 || startx < 1 || endx > 8 || endy < 1) {
				System.out.println("\nIllegal move, try again\n");
				getInput();
				break;
			} else if (boards[startx][starty] == null) {
				System.out.println("\nIllegal move, try again\n");
				getInput();
				break;				
			} else if (move != boards[startx][starty].getColor()) {
				System.out.println("\nIllegal move, try again\n");
				getInput();
				break;
			} else {
				List<String> possiblemoves= boards[startx][starty].getMoves(boards);
				if (possiblemoves.contains(holder[1])) {
					temp = boards[endx][endy];
					boards[endx][endy] = boards[startx][starty];
					boards[startx][starty] = null;
					
					if(boards[endx][endy] != null && boards[endx][endy].getClass() == Pawn.class) {   
						((Pawn)boards[endx][endy]).setBoard(boards);
						
						if(move == ChessPiece.WHITE && (starty - endy) == -2 && boards[endx][endy].getClass() == Pawn.class) {
							EnPassantOkWHITE = true;
						
						}
						if(move == ChessPiece.BLACK && (starty - endy) == 2 && boards[endx][endy].getClass() == Pawn.class) {
							EnPassantOkBLACK = true;
						
						}
				
						if(move == ChessPiece.WHITE && boards[endx][endy - 1] != null && EnPassantOkWHITE == true)  {
								if(boards[endx][endy - 1].getColor() == ChessPiece.BLACK && boards[endx][endy - 1].getClass() == Pawn.class)
								boards[endx][endy - 1] = null;
								EnPassantOkWHITE = false;
							
						}else if (move == ChessPiece.BLACK && boards[endx][endy + 1] != null && EnPassantOkBLACK == true) {
								if( boards[endx][endy + 1].getColor() == ChessPiece.WHITE && boards[endx][endy + 1].getClass() == Pawn.class)
								boards[endx][endy + 1] = null;
								EnPassantOkBLACK = false;
						}
					}
				
					
					
	
					if(boards[endx][endy].getClass() == King.class && (startx - endx) == -2) {
						cancastle = true;
						
					}else if(boards[endx][endy].getClass() == King.class && (startx - endx) == 2)
						cancastle = true;
					
					if(cancastle == true) {
						if((startx - endx) == -2){
							if(move == ChessPiece.BLACK) {
							boards[endx - 1][endy] = boards[8][8];
							boards[8][8] = null;
							cancastle = false;
							}else if(move == ChessPiece.WHITE) {
								boards[endx - 1][endy] = boards[8][1];
								boards[8][1] = null;
								cancastle = false;
							}
						}
						if((startx - endx) == 2) {
							if(move == ChessPiece.BLACK) {
							boards[endx + 1][endy] = boards[1][8];
							boards[1][8] = null;
							cancastle = false;
							}else if (move == ChessPiece.WHITE) {
								boards[endx + 1][endy] = boards[1][1];
								boards[1][1] = null;
								cancastle = false;
							}
						}
						
					}
					
						
					correctinput = true;
					boards[endx][endy].setLocation(endy, endx, holder[1].charAt(0));
					/*
					List<String> test = boards[endx][endy].getMoves(boards);
					for (String s: test) {
						System.out.println("MOVE: " + boards[endx][endy].toString() + " to " + s);
					}
					*/
					
					checkagain(); 
					
					if (move == ChessPiece.WHITE && whitecheck == true) {
						System.out.println("\nIllegal move, try again\n");
						boards[startx][starty] = boards[endx][endy];
						boards[endx][endy] = temp;
						correctinput = false;
						boards[startx][starty].setLocation(starty, startx, holder[0].charAt(0));
						getInput();
						break;
					}
					if (move == ChessPiece.BLACK && blackcheck == true) {
						System.out.println("\nIllegal move, try again\n");
						boards[startx][starty] = boards[endx][endy];
						boards[endx][endy] = temp;
						correctinput = false;
						boards[startx][starty].setLocation(starty, startx, holder[0].charAt(0));
						getInput();
						break;
						
					}
					check();
					
					if (move == ChessPiece.WHITE && blackcheck == true) {
						//System.out.println("Black King is in Check");
					}
					if (move == ChessPiece.BLACK && whitecheck == true) {
						//System.out.println("White King is in Check");
					}
					move = (move == ChessPiece.WHITE) ? ChessPiece.BLACK : ChessPiece.WHITE;
					if (blackcheck == true || whitecheck == true) {
						checkmate();
					}
					break;
				} else {
					System.out.println("\nIllegal move, try again\n");
					getInput();
					break;				
				}
				
			}
		}
	}
	/**
	 * This method generates a fresh chess gameboard
	 * with all pieces in their correct place
	 * @return nothing
	 */
	private void generateBoard() {
		char ascii = 97;
		for (int i = 1; i <= 8; i++) {
			boards[i][2] = new Pawn(String.valueOf(ascii) + "" + 2, ChessPiece.WHITE);
			boards[i][7] = new Pawn(String.valueOf(ascii) + "" + 7, ChessPiece.BLACK);
			ascii++;
		}
		boards[1][1] = new Rook("a1", ChessPiece.WHITE);
		boards[8][1] = new Rook("h1", ChessPiece.WHITE);
		boards[2][1] = new Knight("b1", ChessPiece.WHITE);
		boards[7][1] = new Knight("g1", ChessPiece.WHITE);
		boards[3][1] = new Bishop("c1", ChessPiece.WHITE);
		boards[6][1] = new Bishop("f1", ChessPiece.WHITE);
		boards[4][1] = new Queen("d1", ChessPiece.WHITE);
		boards[5][1] = new King("e1", ChessPiece.WHITE);
		
		boards[1][8] = new Rook("a8", ChessPiece.BLACK);
		boards[8][8] = new Rook("h8", ChessPiece.BLACK);
		boards[2][8] = new Knight("b8", ChessPiece.BLACK);
		boards[7][8] = new Knight("g8", ChessPiece.BLACK);
		boards[3][8] = new Bishop("c8", ChessPiece.BLACK);
		boards[6][8] = new Bishop("f8", ChessPiece.BLACK);
		boards[4][8] = new Queen("d8", ChessPiece.BLACK);
		boards[5][8] = new King("e8", ChessPiece.BLACK);
		
	}
	/**
	 * This method is used to generate a gameboard
	 * that resembles edge cases to check for correctness. This one
	 * generates a board where check can be iniated on the next move.
	 * @return nothing
	 */
	private void generateCheck() {
		boards[6][6] = new King("f6", ChessPiece.WHITE);
		boards[7][5] = new Queen("g5", ChessPiece.WHITE);
		boards[8][8] = new King("h8", ChessPiece.BLACK);
		//boards[1][3] = new Pawn("a3", ChessPiece.WHITE);
		//boards[1][4] = new Pawn("a4", ChessPiece.BLACK);
		//boards[2][4] = new Pawn("b4", ChessPiece.BLACK);
 	}
	/**
	 * Printboard() simply outputs the current gameboard to console
	 * for the user to see.
	 * @return nothing
	 */
	private void printBoard() {
		int count = 8;
		for (int i = 8; i > 0; i--) {
			for (int j = 1; j <= 8; j++) {
				int modi = i % 2;
				int modj = j % 2;
				if (boards[j][i] == null && (modi == modj)) {
					System.out.print("## ");
				} else if (boards[j][i] == null) {
					System.out.print("   ");
				} else {
					System.out.print(boards[j][i].toString() + " ");
				}
			}
			System.out.print(" " + count);
			count--;
			System.out.println();
		}
		System.out.println(" a  b  c  d  e  f  g  h  ");
		System.out.println();
	}
}
