package pieces;

import java.util.List;
import java.util.ArrayList;

/**
 * Pawn Class
 * @author Kevin Slachta
 * @author Leon J Kim
 *
 */

public class Pawn implements ChessPiece {
	private int locnumber = 0;
	private char locchar = 'a';
	private int loccharnum;
	private boolean firstmove = true;
	private boolean EnPassantOk = false;	
	private ChessPiece[][] board;

	public boolean isEnPassantOk() {
		return EnPassantOk;
	}
	public void setEnPassantOk(boolean enPassantOk) {
		EnPassantOk = enPassantOk;
	}
	
	public ChessPiece[][] getBoard() {
		return board;
	}
	public void setBoard(ChessPiece[][] board) {
		this.board = board;
	}
	//public final static int BLACK = 1;
	//public final static int WHITE = 0;
	int color = 0;
	public Pawn (String start, int color) {
		locnumber = Character.getNumericValue(start.charAt(1));
		locchar = start.charAt(0);
		loccharnum = Character.getNumericValue(start.charAt(0)) % 9;
		//System.out.println("START: " + locchar + " : " + loccharnum);
		//System.out.println("START: " + start);
		this.color = color;
		firstmove = true;
	}
	@Override
	public String getLocation() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setLocation(int locnumber, int loccharnumber, char locchar) {
		//Check if current locnumber is different from new locnumber by 2
		if((this.locnumber - locnumber) == 2) {
			//Check if there is a pawn at locchar +-1.
			//eg b2->b4  check c4 and a4 for black pawn
			
			if(board[loccharnumber - 1][locnumber] != null)
			if(board[loccharnumber - 1][locnumber].getClass() == Pawn.class ){
				((Pawn)board[loccharnumber - 1][locnumber]).setEnPassantOk(true);
			}
			if(board[loccharnumber + 1][locnumber] != null)
			if(board[loccharnumber + 1][locnumber].getClass() == Pawn.class ){
				((Pawn)board[loccharnumber + 1][locnumber]).setEnPassantOk(true);
			}
		}
		if((this.locnumber - locnumber) == -2) {
			//Check if there is a pawn at locchar +-1.
			//eg b2->b4  check c4 and a4 for black pawn
			if(board[loccharnumber - 1][locnumber] != null)
			if(board[loccharnumber - 1][locnumber].getClass() == Pawn.class ){
				((Pawn)board[loccharnumber - 1][locnumber]).setEnPassantOk(true);
			}
			if(board[loccharnumber + 1][locnumber] != null)
			if(board[loccharnumber + 1][locnumber].getClass() == Pawn.class ){
				((Pawn)board[loccharnumber + 1][locnumber]).setEnPassantOk(true);
			}
		}
		
		
		this.locnumber = locnumber;
		this.loccharnum = loccharnumber;
		this.locchar = locchar;
		firstmove = false;
	}
	public List<String> getMoves(ChessPiece[][] board) {
		List<String> output = new ArrayList<>();
		//System.out.println(loccharnum + " : " + locnumber);
		if (firstmove == true) {
			if (this.color == ChessPiece.WHITE)
				if (locnumber + 2 < 9)
					if (board[loccharnum][locnumber + 1] == null) 
						if (board[loccharnum][locnumber + 2] == null)
							output.add(locchar + "" + (locnumber + 2));
			if (this.color == ChessPiece.BLACK)
				if (locnumber - 2 > 0)
					if (board[loccharnum][locnumber - 1] == null)
						if (board[loccharnum][locnumber - 2] == null)
							output.add(locchar + "" + (locnumber - 2));
		}
		
		if(EnPassantOk == true) 
			if(this.color == ChessPiece.WHITE && locnumber == 5) {
				if(board[loccharnum + 1][locnumber] != null && board[loccharnum + 1][locnumber].getColor()!= ChessPiece.WHITE) {
					if(board[loccharnum + 1][locnumber].getClass() == Pawn.class) 
						output.add((char)(locchar + 1) + "" + (locnumber + 1 ));
		        }
				if(board[loccharnum - 1][locnumber] != null && board[loccharnum - 1][locnumber].getColor()!= ChessPiece.WHITE) {
					if(board[loccharnum - 1][locnumber].getClass() == Pawn.class)
						output.add((char)(locchar - 1) + "" + (locnumber + 1 ));
		        }
		}
		if(EnPassantOk == true) 
			if(this.color == ChessPiece.BLACK && locnumber == 4) {
				if(board[loccharnum + 1][locnumber] != null && board[loccharnum + 1][locnumber].getColor()!= ChessPiece.BLACK) {
					if(board[loccharnum + 1][locnumber].getClass() == Pawn.class) 
						output.add((char)(locchar + 1) + "" + (locnumber - 1 ));
		        }
				if(board[loccharnum - 1][locnumber] != null && board[loccharnum - 1][locnumber].getColor()!= ChessPiece.BLACK) {
					if(board[loccharnum - 1][locnumber].getClass() == Pawn.class)
						output.add((char)(locchar - 1) + "" + (locnumber - 1 ));
		        }
		}
		
		if (this.color == ChessPiece.WHITE) {
			//move one space
			if (locnumber + 1 < 9)
				if (board[loccharnum][locnumber + 1] == null)
					output.add(locchar + "" + (locnumber + 1));
			//attack left
			if (loccharnum + 1 < 9)
				if (locnumber + 1 < 9)
					if (board[loccharnum + 1][locnumber + 1] != null)
						output.add((char)(locchar + 1) + "" + (locnumber + 1));
			//attack right
			if (loccharnum - 1 > 0)
				if (locnumber + 1 < 9)
					if (board[loccharnum - 1][locnumber + 1] != null)
						output.add((char)(locchar - 1) + "" + (locnumber + 1));
		}
		if (this.color == ChessPiece.BLACK) {
			//move one space
			if (locnumber - 1 > 0)
				if (board[loccharnum][locnumber - 1] == null)
					output.add(locchar + "" + (locnumber - 1));
			//attack left
			if (loccharnum - 1 > 0)
				if (locnumber - 1 > 0)
					if (board[loccharnum - 1][locnumber - 1] != null)
						output.add((char)(locchar - 1) + "" + (locnumber - 1));
			//attack right
			if (loccharnum + 1 < 9)
				if (locnumber - 1 > 0)
					if (board[loccharnum + 1][locnumber - 1] != null)
						output.add((char)(locchar + 1) + "" + (locnumber - 1));
		}
		
	
		return output;
	}
	public String toString() {
		if (color == ChessPiece.BLACK)
			return "bp";
		else
			return "wp";
	}
	@Override
	public int getColor() {
		return this.color;
	}

}
