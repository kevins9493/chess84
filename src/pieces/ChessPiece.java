package pieces;

import java.util.List;

/**
 * Chess Piece Interface
 * @author Kevin Slachta
 * @author Leon J Kim
 *
 */
public interface ChessPiece {
	public int WHITE = 0;
	public int BLACK = 1;
	/**
	 * getLocation is used to get the board location of a chesspiece.
	 * @return String this returns the location of the chesspiece
	 */
	String getLocation();
	/**
	 * Used to get the name of the piece
	 * @return String This returns the name and color of the chesspiece i.e "wK" for white King
	 */
	String toString();
	/**
	 * Used to get the color of the chess piece
	 * @return int This returns the color of the selected piece where 0 = white and 1 = black
	 */
	int getColor();
	/**
	 * Used to get all the valid moves a chess piece can make
	 * @param board This is the gameboard on which all chess pieces are currently saved
	 * @return List<String> This returns a list of type String of all valid tiles this piece can move to.
	 */
	List<String> getMoves(ChessPiece[][] board);
	/**
	 * used to set the new location of the chesspiece once it has been moved.
	 * @param x - This is the Y value of the piece (1-8) on the chess board
	 * @param y - This is the X value of the piece (a-h) on the chess board
	 * @param c - This is the Character of the piece (a-h) on the chess board
	 */
	void setLocation(int x, int y, char c);
}

