package pieces;

import java.util.ArrayList;
import java.util.List;

/**
 * Queen Class
 * @author Kevin Slachta
 * @author Leon J Kim
 *
 */
public class Queen implements ChessPiece {
	private int locnumber = 0;
	private char locchar = 'a';
	private int loccharnum = 0;
	private int color = 0;
	public Queen(String start, int color) {
		this.color = color;
		locnumber = Character.getNumericValue(start.charAt(1));
		locchar = start.charAt(0);
		loccharnum = Character.getNumericValue(start.charAt(0)) % 9;
		this.color = color;
	}
	@Override
	public String getLocation() {
		// TODO Auto-generated method stub
		return null;
	}

	public String toString() {
		if (color == ChessPiece.BLACK) {
			return "bQ";
		} else {
			return "wQ";
		}
	}
	@Override
	public int getColor() {
		return this.color;
	}
	@Override
	public List<String> getMoves(ChessPiece[][] board) {
		List<String> output = new ArrayList<>();
		//Diagonals
		int northeastx = 9 - loccharnum;
		int northeasty = 9 - locnumber;
		//north-east
		for(int i = 1; i < Math.min(northeastx, northeasty); i++) {
			if (board[loccharnum + i][locnumber + i] == null)
				output.add((char)(locchar + i) + "" + (locnumber + i));
			else if (board[loccharnum + i][locnumber + i].getColor() != color) {
				output.add((char)(locchar + i) + "" + (locnumber + i));
				break;
			}
			else
				break;
		}
		//south-west
		int southwestx = loccharnum;
		int southwesty = locnumber;
		for(int i = 1; i < Math.min(southwestx, southwesty); i++) {
			if (board[loccharnum - i][locnumber - i] == null)
				output.add((char)(locchar - i) + "" + (locnumber - i));
			else if (board[loccharnum - i][locnumber - i].getColor() != color) {
				output.add((char)(locchar - i) + "" + (locnumber - i));
				break;
			}
			else
				break;
		}
		//north-west
		int northwestx = loccharnum;
		int northwesty = 9 - locnumber;
		//System.out.println("NW: " + northwestx + " :Y: " +northwesty + " LOC: " + locnumber);
		for(int i = 1; i < Math.min(northwestx, northwesty); i++) {
			if (board[loccharnum - i][locnumber + i] == null)
				output.add((char)(locchar - i) + "" + (locnumber + i));
			else if (board[loccharnum - i][locnumber + i].getColor() != color) {
				output.add((char)(locchar - i) + "" + (locnumber + i));
				break;
			}
			else
				break;
		}
		//south-east
		int southeastx = 9 - loccharnum;
		int southeasty = locnumber;
		for(int i = 1; i < Math.min(southeastx, southeasty); i++) {
			if (board[loccharnum + i][locnumber - i] == null)
				output.add((char)(locchar + i) + "" + (locnumber - i));
			else if (board[loccharnum + i][locnumber - i].getColor() != color) {
				output.add((char)(locchar + i) + "" + (locnumber - i));
				break;
			}
			else
				break;
		}
		//horizontal/vertical
		for(int i = 1; i < 9 - loccharnum; i++) {
			if (board[loccharnum + i][locnumber] == null)
				output.add((char)(locchar + i) + "" + locnumber);
			else if (board[loccharnum + i][locnumber].getColor() != color) {
				output.add((char)(locchar + i) + "" + locnumber);
				break;
			}
			else
				break;
		}
		//east
		for(int i = 1; i < loccharnum - 1; i++) {
			if (board[loccharnum - i][locnumber] == null)
				output.add((char)(locchar - i) + "" + locnumber);
			else if (board[loccharnum - i][locnumber].getColor() != color) {
				output.add((char)(locchar - i) + "" + locnumber);
				break;
			}
			else
				break;
		}
		//north
		for(int i = locnumber + 1; i < 9; i++) {
			if (board[loccharnum][i] == null)
				output.add((char)locchar + "" + i);
			else if (board[loccharnum][i].getColor() != color) {
				output.add((char)locchar + "" + i);
				break;
			}
			else
				break;
		}
		//south
		for(int i = locnumber - 1; i > 0; i--) {
			if (board[loccharnum][i] == null)
				output.add((char)locchar + "" + i);
			else if (board[loccharnum][i].getColor() != color) {
				output.add((char)locchar + "" + i);
				break;
			} 
			else
				break;
		}
		return output;
	}
	@Override
	public void setLocation(int x, int y, char c) {
		this.locnumber = x;
		this.loccharnum = y;
		this.locchar = c;
	}
}
