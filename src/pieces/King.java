package pieces;

import java.util.ArrayList;
import java.util.List;

/**
 *  <h1> King Class that implements the ChessPiece interface.
 * used to hold and calculate all valid moves of the chess piece that is type King. <h1>
 * @author Kevin Slachta
 * @author Leon J Kim
 *
 */
public class King implements ChessPiece {
	private int locnumber = 0;
	private char locchar = 'a';
	private int loccharnum = 0;
	private int color = 0;
	private boolean firstmove = true;
	private boolean cancastle = true;
	/*
	 * Castle must be set to false under the following conditions:
	 * 	if the king has ever moved
	 * 	if the rook has ever moved
	 * Castle will fail under these conditions:
	 * if the king moves through, out of, or into check
	 */
	public King(String start, int color) {
		this.locnumber = Character.getNumericValue(start.charAt(1));
		this.locchar = start.charAt(0);
		this.color = color;
		this.loccharnum = Character.getNumericValue(start.charAt(0)) % 9;
	}

	@Override
	public String getLocation() {
		// TODO Auto-generated method stub
		return ((char) locchar + "" +  locnumber);
	}

	public String toString() {
		if (color == ChessPiece.BLACK) {
			return "bK";
		} else {
			return "wK";
		}
	}
	@Override
	public int getColor() {
		return this.color;
	}
	@Override
	public List<String> getMoves(ChessPiece[][] board) {
		List<String> output = new ArrayList<>();
		//boolean castlecheck = false;
		/*
		if (cancastle == true) {
			for (int i = loccharnum; i > 1; i++) {
				if (board[i][locnumber] != null)
					castlecheck = false;
			}
			
		}
		*/

			
		
		//north
		if (locnumber + 1 < 9)
			if (board[loccharnum][locnumber + 1] == null)
				output.add((char) (locchar) + "" + (locnumber + 1));
			else if (board[loccharnum][locnumber + 1].getColor() != this.color)
				output.add((char) (locchar) + "" + (locnumber + 1));
		//northeast
		if (locnumber + 1 < 9)
			if (loccharnum + 1 < 9)
				if (board[loccharnum + 1][locnumber + 1] == null)
					output.add((char) (locchar + 1) + "" + (locnumber + 1));
				else if (board[loccharnum + 1][locnumber + 1].getColor() != this.color)
					output.add((char) (locchar + 1) + "" + (locnumber + 1));
		//northwest
		if (locnumber + 1 < 9)
			if (loccharnum - 1 > 0)
				if (board[loccharnum - 1][locnumber + 1] == null)
					output.add((char) (locchar - 1) + "" + (locnumber + 1));
				else if (board[loccharnum - 1][locnumber + 1].getColor() != this.color)
					output.add((char) (locchar - 1) + "" + (locnumber + 1));
		//south
		if (locnumber - 1 > 0)
				if (board[loccharnum][locnumber - 1] == null)
					output.add((char) (locchar) + "" + (locnumber - 1));
				else if (board[loccharnum][locnumber - 1].getColor() != this.color)
					output.add((char) (locchar) + "" + (locnumber - 1));
		//southeast
		if (locnumber - 1 > 0)
			if (loccharnum + 1 < 9)
				if (board[loccharnum + 1][locnumber - 1] == null)
					output.add((char) (locchar + 1) + "" + (locnumber - 1));
				else if (board[loccharnum + 1][locnumber - 1].getColor() != this.color)
					output.add((char) (locchar + 1) + "" + (locnumber - 1));
		//southwest
		if (locnumber - 1 > 0)
			if (loccharnum - 1 > 0)
				if (board[loccharnum - 1][locnumber - 1] == null)
					output.add((char) (locchar - 1) + "" + (locnumber - 1));
				else if (board[loccharnum - 1][locnumber - 1].getColor() != this.color)
					output.add((char) (locchar - 1) + "" + (locnumber - 1));
		//west
		if (loccharnum - 1 > 0)
			if (board[loccharnum - 1][locnumber] == null)
				output.add((char) (locchar - 1) + "" + (locnumber));
		
			else if (board[loccharnum - 1][locnumber].getColor() != this.color)
				output.add((char) (locchar - 1) + "" + (locnumber));
		

		if(firstmove == true && cancastle == true) {
			if(color == ChessPiece.WHITE && locnumber == 1 && loccharnum == 5) {
				if(board[loccharnum - 4][locnumber] != null)
				if(board[loccharnum - 4][locnumber].getClass() == Rook.class) {
					for(int i = 1; i < 4; i++) {
						if (board[loccharnum - i][locnumber] == null) {
							output.add((char)(locchar - 2) + "" + (locnumber));
						}
					}
				}
				if(board[loccharnum + 3][locnumber] != null)
				if(board[loccharnum + 3][locnumber].getClass() == Rook.class) {
					for(int j = 1; j < 3; j++) {
						if(board[loccharnum + j][locnumber] == null) {
							output.add((char)(locchar + 2) + "" + (locnumber));
						}
					}
				}
			}
		}
		
		if(firstmove == true && cancastle == true) {
			if(color == ChessPiece.BLACK && locnumber == 8 && loccharnum == 5) {
				if(board[loccharnum - 4][locnumber] != null)
				if(board[loccharnum - 4][locnumber].getClass() == Rook.class) {
					for(int i = 1; i < 4; i++) {
						if (board[loccharnum - i][locnumber] == null) {
							output.add((char)(locchar - 2) + "" + (locnumber));

						}
					}
				}
				if(board[loccharnum + 3][locnumber] != null)
				if(board[loccharnum + 3][locnumber].getClass() == Rook.class) {
					for(int j = 1; j < 3; j++) {
						if(board[loccharnum + j][locnumber] == null) {
							output.add((char)(locchar + 2) + "" + (locnumber));
						}
					}
				}
			}
		}
		
					

		
		
		//east
		if (loccharnum + 1 < 9)
			if (board[loccharnum + 1][locnumber] == null)
				output.add((char) (locchar + 1) + "" + (locnumber));
			else if (board[loccharnum + 1][locnumber].getColor() != this.color)
				output.add((char) (locchar + 1) + "" + (locnumber));
		return output;
	}
	@Override
	public void setLocation(int x, int y, char c) {
		this.locnumber = x;
		this.loccharnum = y;
		this.locchar = c;
		cancastle = false;
		firstmove = false;
	}
}
