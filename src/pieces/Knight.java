package pieces;

import java.util.ArrayList;
import java.util.List;

/**
 * Knight Class
 * @author Kevin Slachta
 * @author Leon J Kim
 *
 */
public class Knight implements ChessPiece {
	private int locnumber = 0;
	private char locchar = 'a';
	private int loccharnum = 0;
	private int color = 0;
	public Knight(String start, int color) {
		locnumber = Character.getNumericValue(start.charAt(1));
		locchar = start.charAt(0);
		loccharnum = Character.getNumericValue(start.charAt(0)) % 9;
		this.color = color;
	}
	@Override
	public String getLocation() {
		// TODO Auto-generated method stub
		return null;
	}
	public String toString() {
		if (color == ChessPiece.BLACK)
			return "bN";
		else
			return "wN";
	}
	@Override
	public int getColor() {
		return this.color;
	}
	@Override
	public List<String> getMoves(ChessPiece[][] board) {
		List<String> output = new ArrayList<>();
		//L - south-east
		if (loccharnum + 1 < 9)
			if (locnumber - 2 > 0)
				if (board[loccharnum + 1][locnumber - 2] == null)
					output.add((char)(locchar + 1) + "" + (locnumber - 2));
				else 
					if (board[loccharnum + 1][locnumber - 2].getColor() != this.color)
						output.add((char)(locchar + 1) + "" + (locnumber - 2));				
		//reverse L - south-west
		if (loccharnum - 1 > 0)
			if (locnumber - 2 > 0)
				if (board[loccharnum - 1][locnumber - 2] == null)
					output.add((char)(locchar - 1) + "" + (locnumber - 2));
				else 
					if (board[loccharnum - 1][locnumber - 2].getColor() != this.color)
						output.add((char)(locchar - 1) + "" + (locnumber - 2));		
		//L-inverted - northeast
		if (loccharnum + 1 < 9)
			if (locnumber + 2 < 9)
				if (board[loccharnum + 1][locnumber + 2] == null)
					output.add((char)(locchar + 1) + "" + (locnumber + 2));
				else 
					if (board[loccharnum + 1][locnumber + 2].getColor() != this.color)
						output.add((char)(locchar + 1) + "" + (locnumber + 2));
		//reverse L-inverted - northwest
		if (loccharnum - 1 > 0)
			if (locnumber + 2 < 9)
				if (board[loccharnum - 1][locnumber + 2] == null)
					output.add((char)(locchar - 1) + "" + (locnumber + 2));
				else 
					if (board[loccharnum - 1][locnumber + 2].getColor() != this.color)
						output.add((char)(locchar - 1) + "" + (locnumber + 2));		
		return output;
	}
	@Override
	public void setLocation(int x, int y, char c) {
		this.locnumber = x;
		this.loccharnum = y;
		this.locchar = c;
	}
}
