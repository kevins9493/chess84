package pieces;

import java.util.ArrayList;
import java.util.List;

/**
 * Rook Class
 * @author Kevin Slachta
 * @author Leon J Kim
 *
 */

public class Rook implements ChessPiece {
	private int locnumber;
	private char locchar;
	private int loccharnum;
	private int color;
	public Rook (String start, int color) {
		this.color = color;
		locnumber = Character.getNumericValue(start.charAt(1));
		locchar = start.charAt(0);
		loccharnum = Character.getNumericValue(start.charAt(0)) % 9 ;
		//System.out.println(loccharnum);
		this.color = color;
	}
	@Override
	public String getLocation() {
		// TODO Auto-generated method stub
		return null;
	}

	public String toString() {
		if (color == ChessPiece.BLACK) {
			return "bR";
		} else {
			return "wR";
		}
	}
	@Override
	public int getColor() {
		return this.color;
	}
	@Override
	public List<String> getMoves(ChessPiece[][] board) {
		List<String> output = new ArrayList<>();
		//west
		for(int i = 1; i < 9 - loccharnum; i++) {
			if (board[loccharnum + i][locnumber] == null)
				output.add((char)(locchar + i) + "" + locnumber);
			else if (board[loccharnum + i][locnumber].getColor() != color) {
				output.add((char)(locchar + i) + "" + locnumber);
				break;
			}
			else
				break;						
			}
			
		//east
		for(int i = 1; i < loccharnum - 1; i++) {
			if (board[loccharnum - i][locnumber] == null)
				output.add((char)(locchar - i) + "" + locnumber);
			else if (board[loccharnum - i][locnumber].getColor() != color) {
				output.add((char)(locchar - i) + "" + locnumber);
				break;
			}
			else
				break;
		}
		//north
		for(int i = locnumber + 1; i < 9; i++) {
			if (board[loccharnum][i] == null)
				output.add((char)locchar + "" + i);
			else if (board[loccharnum][i].getColor() != color) {
				output.add((char)locchar + "" + i);
				break;
			}
			else
				break;
		}
		//south
		for(int i = locnumber - 1; i > 0; i--) {
			if (board[loccharnum][i] == null)
				output.add((char)locchar + "" + i);
			else if (board[loccharnum][i].getColor() != color) {
				output.add((char)locchar + "" + i);
				break;
			} 
			else
				break;
		}
		return output;
	}
	@Override
	public void setLocation(int x, int y, char c) {
		this.locnumber = x;
		this.loccharnum = y;
		this.locchar = c;
	}
}
